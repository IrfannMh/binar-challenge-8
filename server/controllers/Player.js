'use strict';

var utils = require('../utils/writer.js');
var Player = require('../service/PlayerService');

module.exports.getPlayers = function getPlayers (req, res, next, point, level) {
  Player.getPlayers(point, level)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
