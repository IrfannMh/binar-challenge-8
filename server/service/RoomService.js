'use strict';


/**
 * Challenge other player
 * Challenge other player
 *
 * userId Integer 
 * returns inline_response_200
 **/
exports.challengeOpponent = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "gameId" : 1,
    "nameRoom" : "room 1",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "waiting for opponent"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Create Room
 *
 * body Object 
 * returns inline_response_200_4
 **/
exports.createRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : {
    "player1Id" : "1",
    "nameRoom" : "room 1",
    "player2Id" : "Waiting player 2",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "open"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get Rooms
 * Get all Rooms
 *
 * isOpen Boolean Filter room isOpen (optional)
 * returns inline_response_200_3
 **/
exports.getRoom = function(isOpen) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : [ {
    "gameId" : 1,
    "nameRoom" : "room 1",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "isOpen"
  }, {
    "gameId" : 1,
    "nameRoom" : "room 1",
    "created_at" : "30-07-2021 12:00 +07:00",
    "status" : "isOpen"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Player Join Room
 * Player 2 join Room
 *
 * roomId Integer 
 * returns inline_response_200_2
 **/
exports.joinRoom = function(roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "player1Id" : "1",
  "nameRoom" : "room1",
  "player2Id" : "2",
  "status" : "Closed"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

