'use strict';


/**
 * Get Biodata
 * Get Biodata by id
 *
 * biodataId Integer 
 * returns inline_response_200_5
 **/
exports.getBiodata = function(biodataId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "phoneNumber" : "81223334444",
    "address" : "Jakarta",
    "avatarURL" : "https://www.google.com/search?q=swagger&sxsrf=M",
    "fullName" : "User 1 Rock Paper Scissor",
    "bio" : "Best player game rock paper scissor"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update Biodata
 * You can update your biodata if you feel like there's something need to be updated.
 *
 * body Object 
 * biodataId Integer 
 * returns inline_response_200_5
 **/
exports.updateBiodata = function(body,biodataId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "phoneNumber" : "81223334444",
    "address" : "Jakarta",
    "avatarURL" : "https://www.google.com/search?q=swagger&sxsrf=M",
    "fullName" : "User 1 Rock Paper Scissor",
    "bio" : "Best player game rock paper scissor"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

